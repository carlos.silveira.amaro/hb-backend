source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
ruby '2.7.0' # Ruby
gem 'rails', '>= 5.0.0.racecar1', '< 5.1'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.0'
gem 'bigdecimal', '1.4.2'
gem 'active_model_serializers', '~> 0.10.0', require: true
gem 'kaminari'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.0'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

gem 'devise'
gem 'paperclip', '~> 6.0.0'
gem 'aws-sdk'
gem 'tiddle'

gem 'sidekiq'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'annotate', github: 'ctran/annotate_models'
  gem 'pry'
end

group :development do
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  gem 'capistrano', '~> 3.5.0'
  gem 'capistrano-rails', '~> 1.1', require: false
  gem 'rvm1-capistrano3', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

group :staging do
  gem 'unicorn', '~>5.1.0'
end

# Redis for Rails
gem 'redis-rails'
gem 'redis-namespace'


gem 'faker', :git => 'https://github.com/stympy/faker.git', :branch => 'master'

# Geocoder
gem 'geokit'
gem 'geokit-rails'

# Pagination
gem 'will_paginate'

# New relic
gem 'newrelic_rpm'

# Prerender
gem 'prerender_rails'

# Social login
gem 'rack-cors'
# gem 'google-api-client'
gem 'jwt'
gem 'signet'

#SENDGRID
gem 'sendgrid-ruby'
