class ButtonNetsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show, :allow]
  before_action :set_button_net, only: [:show, :destroy, :update, :allow]

  def index
    if !params[:name].nil?
      if params[:filter].nil?
        button_net = ButtonNet.find_by(name: params[:name])
        render json: button_net,
               serializer: ButtonNets::ShowSerializer
      else
        if current_user.nil?
          button_nets = ButtonNet.all.where("name ILIKE ? AND privacy = false", "%#{params[:name]}%")
        else
          button_nets1 = ButtonNet.all.where("name ILIKE ? AND privacy = false", "%#{params[:name]}%")
          button_nets2 = button_nets1 + ButtonNet.all.where("privacy = true AND allowed_users && ARRAY[?]", [current_user.id])
          button_nets = button_nets2 + ButtonNet.all.where("privacy = true AND creator_id = ?", [current_user.id])
        end
        render json: button_nets,
               each_serializer: ButtonNets::IndexSerializer
      end
    else
      if !params[:filter].nil? && !params[:creator_id].nil? && !current_user.nil?
        button_nets = ButtonNets::Query.exec params, current_user
      elsif !params[:filter].nil? && !params[:filter][:admin].nil? && params[:filter][:admin] && !current_user.nil?
        button_nets = ButtonNet.all.where("admin_users && ARRAY[?]", [current_user.id])
      else
        button_nets = ButtonNets::Query.exec params, current_user
      end
      render json: button_nets,
             each_serializer: ButtonNets::IndexSerializer
    end
  end

  def create
    button_net = ButtonNets::Create.exec params, current_user
    if button_net.save
      render json: button_net,
             serializer: ButtonNets::ShowSerializer

      #send mail to superadmin to accept
      # UserNotifierMailer.new_button_net(button_net.creator, button_net).deliver
    else
      respond_with_errors(button_net)
    end
  end

  def update
    respond_with_unauthorized and return if current_user.blank? || !(@button_net.creator == current_user || @button_net.admin_users.include?(current_user.id))
    button_net = ButtonNets::Update.exec params, @button_net, current_user
    if button_net.save
      render json: button_net,
             serializer: ButtonNets::ShowSerializer
    else
      respond_with_errors(user)
    end
  end

  def allow
    respond_with_unauthorized and return if current_user.blank?
    arr = @button_net.allowed_users;
    if !arr.include?(current_user.id)
      arr.push(current_user.id)
    end
    if @button_net.update(allowed_users: arr);
      render json: @button_net,
             serializer: ButtonNets::ShowSerializer
    else
      respond_with_errors(user)
    end
  end



  def show
    render json: @button_net,
           serializer: ButtonNets::ShowSerializer
  end

  def destroy
    if @button_net.destroy
      respond_with_ok
    else
      respond_with_errors(@button_net)
    end
  end

  private
  #accept function to be called by the superadmins
  def accept
    @button_net = Resources::Get.exec params
     # UserNotifierMailer.accepted_button_net(button_net.creator, check_mailer).deliver
  end

  #reject function to be called by the superadmins
  def reject
    @button_net = Resources::Get.exec params
     # UserNotifierMailer.rejected_button_net(button_net.creator, check_mailer).deliver
  end

  def set_button_net
    @button_net = Resources::Get.exec params
    respond_with_not_found(button_net.name) if @button_net.nil?
  end

end
