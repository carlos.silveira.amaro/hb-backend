class Buttons::ButtonNetsController < ApplicationController
  before_action :set_button

  def add_button_nets
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
      new_button_net = ButtonNet::Retrieve.exec params
      @button.button_net << (new_button_net - @button.button_net)
    respond_with_ok
  end

  def remove_button_nets
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
      old_button_net = ButtonNet::Retrieve.exec params
      @button.button_net.destroy(old_button_net - (old_button_net - @button.button_net))
    respond_with_ok
  end

  def update_button_nets
    # respond_with_unauthorized and return if current_user.blank? || current_user != @button.creator
      updated_button_net = ButtonNet::Retrieve.exec params
      @button.button_net.clear
      @button.button_net << updated_button_net
    respond_with_ok
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_button
    @button = Resources::Get.exec params
    respond_with_not_found(Button.name) if @button.nil?
  end

end
