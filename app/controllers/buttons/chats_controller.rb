class Buttons::ChatsController < ApplicationController
  before_action :set_button, only: [:sharer_chats, :sharer_chats_relation]

  def sharer_chats
    chats = get_button_chats
    unless chats.blank?
      render json: chats,
             include: ['button_creator.*','user.*','button.*'],
             current_user: current_user,
             each_serializer: Chat::ShowSerializer
    else
      respond_with_empty
    end
  end

  def sharer_chats_relation
    chats = get_button_chats
    unless chats.blank?
      render json: chats,
             current_user: current_user,
             each_serializer: Chat::NestedIndexSerializer
    else
      respond_with_empty
    end
  end

  private

  def get_button_chats
    return [] if current_user.blank?
    current_user==@button.creator ? @button.chats : @button.chats.sharer?(current_user)
  end

  def set_button
    @button = Resources::Get.exec params
    respond_with_not_found(Button.name) if @button.nil?
  end

end
