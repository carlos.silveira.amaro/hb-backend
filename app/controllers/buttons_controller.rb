class ButtonsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :set_button, only: [:show, :update, :destroy]

  # GET /buttons
  def index
    if params[:buttonNet]
      if current_user.nil?
        buttons,meta_info = Buttons::Search.exec params, nil
      else
        buttons,meta_info = Buttons::Search.exec params, current_user
      end
    else
      if current_user.nil?
        buttons,meta_info = Buttons::Search.exec params, nil
      else
        buttons,meta_info = Buttons::Search.exec params, current_user
      end
    end
    tags = []
    if !buttons.nil?
      buttons.each do |button|
        button.offer_tags.each do |offer|
          tags.push(offer.id);
        end
        button.needed_tags.each do |need|
          tags.push(need.id);
        end
      end
    end
    tagsCounter = Hash.new(0)

    if !tags.nil?
      tags.each do |v|
        tagsCounter[v] += 1
      end
    end
    tagsCounter = Hash[tagsCounter.sort_by{|k, v| v}.reverse]
    if meta_info.nil? || (meta_info.class.to_s == 'Button' && !params[:buttonNet].nil?) || (!buttons.nil? && meta_info == buttons.first) || (buttons.nil? && !params[:buttonNet].nil?)
      if buttons.nil?
        buttons = Button.all.where(id: 99999999);
      end
      meta_info = {}
      meta_info[:lat] = ButtonNet.find_by(name: params[:buttonNet]).latitude
      meta_info[:lng] = ButtonNet.find_by(name: params[:buttonNet]).longitude
      meta_info[:tagsCounter] = 0
      # buttons
      # @btns = Button.where('button_net_id && ARRAY[?]', [ButtonNet.find_by(name: params[:buttonNet]).id])
      if params[:page]
        buttons2 = buttons.page(params[:page]).per_page(params[:size])
      end
      render json: buttons,
             include: include_for_button_search,
             meta: meta_info, each_serializer: Button::IndexSerializer
    else

      if buttons.nil?
        buttons = Button.all.where(id: 99999999);
      end
      if meta_info.class.to_s == 'Button'
        lastMeta = meta_info
        meta_info = {}
        meta_info[:lat] = lastMeta.latitude
        meta_info[:lng] = lastMeta.longitude
        meta_info[:tagsCounter] = 0
      end
      meta_info[:tagsCounter] = tagsCounter
      # buttons = buttons.page(params[:page]).per_page(params[:size])
      render json: buttons,
             include: include_for_button_search,
             meta: meta_info, each_serializer: Button::IndexSerializer
    end
  end

  # GET /buttons/1
  def show
    render json: @button,
           include: include_for_button,
           serializer: Button::ShowSerializer
  end

  # POST /buttons
  def create
    button = Buttons::Create.exec params, current_user
    if button.save
        check_mailer = if button.active == true && button.active_was == false
                        true
                      elsif button.active == false && button.active_was == true
                        false
                      else
                        nil
                      end
        UserNotifierMailer.new_button(current_user, button).deliver

      paramsDeserialized = ActiveModelSerializers::Deserialization.jsonapi_parse params
      offerTagsDeserialized = paramsDeserialized[:offer_tag_ids]
      neededTagsDeserialized = paramsDeserialized[:needed_tag_ids]

      render json: button, status: :created,
             include: include_for_button,
             serializer: Button::ShowSerializer
      if !offerTagsDeserialized.nil?
        offerTagsDeserialized.each do |offer|
          offerObj = Tag.find(offer)
          if !offerObj.users.nil?
            offerObj.users.each do |user|
              userTag = user.user_tags.find_by(tag_id: offer)
              hasButtonNet =  true
              if !userTag.button_nets[0].nil?
                hasButtonNet =  button.button_net_id[0] == userTag.button_nets[0]
              end
              if userTag.tag_type != 0 && hasButtonNet
                if user.deliver_interests
                  if !user.tags_distance.nil? && !user.tags_latitude.nil? && !user.tags_longitude.nil?
                    current_location = Geokit::LatLng.new(user.tags_latitude, user.tags_longitude)
                    destination = button.latitude.to_s + ', ' + button.longitude.to_s
                    distanceFromButton = current_location.distance_to(destination)
                    if distanceFromButton <= user.tags_distance
                      UserNotifierMailer.button_tag(offerObj, user, button).deliver
                    end
                  else
                    # UserNotifierMailer.button_tag(offerObj, user, button).deliver
                  end
                end
              end
            end
          end
        end
      end

      if !neededTagsDeserialized.nil?
        neededTagsDeserialized.each do |needed|
          neededObj = Tag.find(needed)
           if !neededObj.users.nil?
            neededObj.users.each do |user|
              userTag = user.user_tags.find_by tag_id: needed
              hasButtonNet =  true
              if !userTag.button_nets[0].nil?
                hasButtonNet =  button.button_net_id[0] == userTag.button_nets[0]
              end
              if userTag.tag_type != 1 && hasButtonNet
                if user.deliver_interests
                  if !user.tags_distance.nil? && !user.tags_latitude.nil? && !user.tags_longitude.nil?
                    current_location = Geokit::LatLng.new(user.tags_latitude, user.tags_longitude)
                    destination = button.latitude.to_s + ', ' + button.longitude.to_s
                    distanceFromButton = current_location.distance_to(destination)
                    if distanceFromButton <= user.tags_distance
                      UserNotifierMailer.button_tag(neededObj, user, button).deliver
                    end
                  else
                    # UserNotifierMailer.button_tag(neededObj, user, button).deliver
                  end
                end
              end
            end
          end
        end
      end
    else
      respond_with_errors(button)
    end
  end

  # PATCH/PUT /buttons/1
  def update
    if event_params[:event] && event_params[:command] == "button-remove-net"
      @button.button_net_id.each do |id|
        if ButtonNet.find(id).admin_users.include?(current_user.id) || ButtonNet.find(id).creator == current_user
          # if @button.update(button_net_id: @button.button_net_id.delete(params[:data][:attributes][:'is-editable-id']))
          @button.button_net_id.delete(params[:data][:attributes][:'is-editable-id'])
          @button.active = false
          if @button.save
            render json: @button,
                   include: include_for_button,
                   serializer: Button::ShowSerializer
            break
          else
            respond_with_errors(@button)
            break
          end
        end
      end
    else
      if event_params[:event] && event_params[:command] == "button-shared"
        button = @button
        button.shared_counter+=1
      else
        unless current_user.admin || current_user.super_admin || (event_params[:event] && event_params[:command] == "button-transfered")
          respond_with_unauthorized and return if !current_user || current_user.id != @button.creator_id
        end
        button = Buttons::Update.exec params, @button
        respond_with(I18n.t("activerecord.errors.buttons.active.uncomplete"), 403, I18n.t("activerecord.errors.buttons.active.uncomplete_description")) and return if button==:unathorized
      end
      if button.save
        button.tags.each do |tag|
          tag.update_counter
          tag.save
        end

        unless event_params[:event] && event_params[:command] == "button-shared"
              if event_params[:event] && event_params[:command] == "button-transfered"
              UserNotifierMailer.transfered_button(button.creator, button).deliver
              end
        end

        render json: button,
               include: include_for_button,
               serializer: Button::ShowSerializer
      else
        respond_with_errors(button)
      end
    end
  end

  # DELETE /buttons/1
  def destroy
    unless current_user.admin || current_user.super_admin
      # if current_user.nil? || (current_user!=@button.creator)
      #   @button.button_net_id.each do |id|
      #     if ButtonNet.find(id).admin_users.include?(current_user.id) || ButtonNet.find(id).creator == current_user
      #       if @button.update(button_net_id: @button.delete(params[:delete_id])), current_user
      #         respond_with_ok
      #       else
      #         respond_with_errors(@button)
      #       end
      #     end
      #   end
      # end
      respond_with_unauthorized and return if current_user.nil? || (current_user!=@button.creator)
    end
    if Buttons::Delete.exec @button, current_user
      respond_with_ok
    else
      respond_with_errors(@button)
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_button
    @button = Resources::Get.exec params
    respond_with_not_found(Button.name) if @button.nil?
  end

  def include_for_button
    ['image', 'creator', 'creator.avatar', 'offer_tags', 'needed_tags']
  end

  def include_for_button_search
    ['image', 'creator', 'creator.avatar', 'offer_tags', 'needed_tags', 'search_trending_tags']
  end

  def event_params
    action_event = ActiveModelSerializers::Deserialization.jsonapi_parse params, only: %i[ action-event ]
    return {
      event: !action_event.try(:[], :action_event).blank?,
      command: action_event.try(:[], :action_event)
    }
  end

end
