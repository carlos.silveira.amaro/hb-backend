module Errors
  extend ActiveSupport::Concern

  def respond_with_errors(object, status = 422)
    render json: serialized_errors(object), status: status
    # render json: object, status: status, adapter: :jsonapi #, serializer: ActiveModel::Serializer::ErrorSerializer
  end

  def respond_with_not_found(class_name=nil)
    render json: {errors: [{
        title: I18n.t("activerecord.errors.resource.not_found", class_name: class_name || "Resource"),
        detail: I18n.t("activerecord.errors.resource.not_found_description")}]
    }, status: 404
  end

  def respond_with_unauthorized
    render json: {errors: [{
        title: I18n.t("activerecord.errors.messages.access_denied"),
        detail: I18n.t("activerecord.errors.messages.access_denied_description")}]
    }, status: 403
  end

  def respond_with(title, status, detail=nil)
    render json: {errors: [{
        title: title,
        detail: detail}]
    }, status: status
  end

  def respond_with_ok
    render json: {}, status: 204
  end

  def respond_with_empty
    render json: { data: [] }
  end

  def respond_with_internal_error
    render json: {error: ['Error occurred']}, status: :internal_server_error
  end

  private

  def serialized_errors(model)
    {errors: model.errors.messages.map{|k,v| {detail: I18n.t("activerecord.attributes.#{model.class.name.downcase}.#{k.to_s}") + " #{v[0]}", source: {pointer: "data/attributes/"+k.to_s.tr("_", "-")}}}}
  end

end
