class HomeController < ApplicationController
# ---------------------------------------------------------------------------- #
#  => Class: HomeController                                                    #
#  Description: Allows you to redirect router to client                        #
# ---------------------------------------------------------------------------- #

# ---------------------------------------------------------------------------- #
#       Method: index                                                          #
#   Attributes: none                                                           #
#       Params: none                                                           #
#  Description: Used when you redirect in the app.                             #
# ---------------------------------------------------------------------------- #
  def index
    unless Rails.env.development?
      html_string = REDIS.get("help-buttons-app:index:current-content")
      render :html => html_string.html_safe
    end
  end

end
