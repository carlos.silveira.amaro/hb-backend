class MessagesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_message, only: [:show, :destroy]


  def index
    messages = Messages::Query.exec params
    if messages
      render json: messages,
              include: include_for_message,
             each_serializer: Message::ShowSerializer
    else
      respond_with_empty
    end
  end

  def create
    time = 0
    message = Messages::Create.exec params, current_user
    respond_with(I18n.t("activerecord.errors.buttons.deactivated_user.deactivated"), 403, I18n.t("activerecord.errors.buttons.deactivated_user.deactivated_description")) and return if message==:unathorized
    respond_with(I18n.t("activerecord.errors.buttons.active.disabled"), 403, I18n.t("activerecord.errors.buttons.active.disabled_description")) and return if message==:disabled

    chat = Chat.find(params[:data][:relationships][:chat][:data][:id])

    if !chat.messages.order(created_at: :asc).last.nil?
      time =  Time.now.to_i - chat.messages.order(created_at: :asc).last.created_at.to_i
    else
      time = 31
    end

    if message.save
      render json: message,
              include: include_for_message,
             serializer: Message::ShowSerializer
       if time > 30
         UserNotifierMailer.new_message(current_user, message).deliver
       end
    else
      respond_with_errors(message)
    end

  end

  def show
    render json: @message,
           include: include_for_message,
           serializer: Message::ShowSerializer
  end

  def destroy
    if @message.destroy
      respond_with_ok
    else
      respond_with_errors(@message)
    end
  end

  private

  def set_message
    @message = Resources::Get.exec params
    respond_with_not_found(Message.name) if @message.nil?
  end

  def include_for_message
    ['user.avatar', 'chat']
  end

end
