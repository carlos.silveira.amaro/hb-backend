class UserTagsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy, :update]
  before_action :set_tag, only: [:show, :destroy, :update]

  def index
    tags = UserTags::Query.exec params
    render json: tags,
           each_serializer: UserTag::IndexSerializer
  end

  def create
    tag = UserTags::Create.exec params
    # tag.search_name = I18n.transliterate(tag.name.downcase)
    tag.update(user_id: current_user.id)
    if tag.save
      render json: tag,
             serializer: UserTag::ShowSerializer
    else
      respond_with_errors(tag)
    end
  end

  def show
    render json: @tag,
           serializer: UserTag::ShowSerializer
  end

  def update
    respond_with_not_found and return if @tag.blank?
    respond_with_unauthorized and return if @tag.user != current_user
    tag = UserTags::Update.exec params, @tag
    if tag.save
      render json: tag,
             serializer: UserTag::ShowSerializer
    else
      respond_with_errors(tag)
    end
  end

  def destroy
    if @tag.destroy
      respond_with_ok
    else
      respond_with_errors(@tag)
    end
  end

  private

  def set_tag
    @tag = Resources::Get.exec params
    respond_with_not_found(UserTag.name) if @tag.nil?
  end

end
