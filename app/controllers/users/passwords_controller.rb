class Users::PasswordsController < Devise::PasswordsController

  respond_to :json

  def create
    user = User.find_by_email params["email"]
    respond_with(I18n.t("activerecord.errors.recovery.user_not_found"), 404, I18n.t("activerecord.errors.recovery.user_not_found_description")) and return if user.blank?
    self.resource = User.send_reset_password_instructions(user.attributes)
    if successfully_sent?(resource)
      respond_with_ok
    else
      respond_with_internal_error
    end
  end

  def reset
    unless params[:password].present? && params[:'password-confirmation'].present?
      self.resource = User.new
      resource.errors.add(:base, I18n.t('activerecord.errors.resource.fill_all_fields')) if params[:password].blank? || params[:'password-confirmation'].blank?
    else
      self.resource = User.reset_password_by_token(attributes={reset_password_token: params[:token], password: params[:password], password_confirmation: params[:'password-confirmation']})
      yield resource if block_given?
    end
    if resource.errors.empty?
      resource.unlock_access! if unlockable?(resource)
      if Devise.sign_in_after_reset_password
        flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
        set_flash_message!(:notice, flash_message)
        sign_in(resource_name, resource)
      else
        set_flash_message!(:notice, :updated_not_active)
      end

      # respond_with resource, location: after_resetting_password_path_for(resource)
      token = Tiddle.create_and_return_token(resource, request)
      render json: { id: resource.id, authentication_token: token }
    else
      set_minimum_password_length
      respond_with_errors(resource)
    end
  end


end
