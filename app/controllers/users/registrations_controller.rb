class Users::RegistrationsController < Devise::RegistrationsController

  respond_to :json

  def create
    user = Users::Create.exec params
    password = user.password
    if user.save
      @transferedButtons = TransferedButton.where(email: user.email)
      @transferedButtons.each do |transfer|
        @button = Button.find(transfer.button_id)
        if @button.creator.id == transfer.user_id
          Button.find(transfer.button_id).update(creator: user)
          transfer.destroy
        end
      end
      user.random_password = password

      UserNotifierMailer.new_user(user, password).deliver
      render json: user,
             meta: { password: password },
             serializer: User::RegistrationSerializer
    else
      respond_with_errors(user)
    end
  end

end
