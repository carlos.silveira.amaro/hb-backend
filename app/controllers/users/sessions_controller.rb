class Users::SessionsController < Devise::SessionsController

  respond_to :json

  def create
    user = warden.authenticate!(auth_options)
    token = Tiddle.create_and_return_token(user, request)
    render json: { id: user.id, authentication_token: token }
  end

  def destroy
    Tiddle.expire_token(current_user, request) if current_user
    respond_with_ok
  end

  private
  def verify_signed_out_user
  end
end
