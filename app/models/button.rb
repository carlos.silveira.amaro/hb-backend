# ## Schema Information
#
# Table name: `buttons`
#
# ### Columns
#
# Name                        | Type               | Attributes
# --------------------------- | ------------------ | ---------------------------
# **`active`**                | `boolean`          | `default(FALSE)`
# **`button_net_id`**         | `integer`          | `default([]), is an Array`
# **`button_type`**           | `integer`          | `default("need")`
# **`created_at`**            | `datetime`         | `not null`
# **`creator_id`**            | `integer`          |
# **`date`**                  | `string`           |
# **`deactivated_user`**      | `boolean`          | `default(FALSE)`
# **`description`**           | `text`             |
# **`full_address`**          | `string`           |
# **`id`**                    | `integer`          | `not null, primary key`
# **`latitude`**              | `float`            |
# **`location_name`**         | `string`           |
# **`longitude`**             | `float`            |
# **`mail_schedule_sended`**  | `boolean`          | `default(FALSE)`
# **`periodic_date`**         | `string`           |
# **`shared_counter`**        | `integer`          | `default(0)`
# **`swap`**                  | `boolean`          |
# **`to_latitude`**           | `float`            |
# **`to_location_name`**      | `string`           |
# **`to_longitude`**          | `float`            |
# **`updated_at`**            | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_buttons_on_button_type`:
#     * **`button_type`**
# * `index_buttons_on_creator_id`:
#     * **`creator_id`**
#
# ### Foreign Keys
#
# * `fk_rails_...`:
#     * **`creator_id => users.id`**
#

class Button < ApplicationRecord

  include Geokit::Geocoders

  acts_as_mappable :lat_column_name => :latitude, :lng_column_name => :longitude

  has_one :image, class_name: 'Image', foreign_key: 'imageable_id' ,as: :imageable, dependent: :destroy
  belongs_to :creator, class_name: 'User'
  has_many :chats, inverse_of: :button, dependent: :destroy
  has_many :button_tags
  has_many :tags, through: :button_tags, source: :tag
  has_many :offer_tags, -> { ButtonTag.offered }, through: :button_tags, source: :tag
  has_many :needed_tags, -> { ButtonTag.needed }, through: :button_tags, source: :tag

  # has_many :button_nets, through: :button_net_id, source: :button_net

  after_save :set_lat_and_long

  enum button_type: { need: 0, offer: 1 , change: 2 }

  scope :shown?, ->(show=nil) { unless show.nil?
    where 'active = ? AND deactivated_user = ?', show, !show
  end }

  scope :shown_admin?, ->(show=nil,current_user=nil) { unless show.nil? || current_user.nil?
    where '(active = ? AND deactivated_user = ?) OR (active = ? AND creator_id = ?)', show, !show, !show, current_user.id
  end }

  scope :shown_super_admin?, ->(show=nil,current_user=nil) { unless show.nil?
    where 'deactivated_user = ?', !show
  end }

  scope :active?, ->(actived=nil) { unless actived.nil?
    where 'active = ?', actived
  end }

  scope :user_active?, ->(activated=nil) { unless activated.nil?
    where 'deactived_user = ?', !activated
  end }


  def set_lat_and_long
    if (self.latitude.blank? or self.longitude.blank?) and self.full_address.present?
      loc = GoogleGeocoder.geocode(self.full_address)
      self.latitude = loc.lat
      self.longitude = loc.lng
      if self.latitude.present? && self.longitude.present? && self.latitude.is_a?(Numeric) && self.longitude.is_a?(Numeric)
        self.save
      end
    end
  end

end
