# ## Schema Information
#
# Table name: `button_tags`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`button_id`**   | `integer`          |
# **`created_at`**  | `datetime`         | `not null`
# **`id`**          | `integer`          | `not null, primary key`
# **`tag_id`**      | `integer`          |
# **`tag_type`**    | `integer`          |
# **`updated_at`**  | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_button_tags_on_button_id`:
#     * **`button_id`**
# * `index_button_tags_on_tag_id`:
#     * **`tag_id`**
#

class ButtonTag < ApplicationRecord
  belongs_to :button
  belongs_to :tag

  scope :offered, -> { where(tag_type: 0) }
  scope :needed, -> { where(tag_type: 1) }

  after_commit :update_tag_counter

  def update_tag_counter
    self.tag.update_counter
  end

end
