# ## Schema Information
#
# Table name: `chats`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`button_id`**   | `integer`          |
# **`created_at`**  | `datetime`         | `not null`
# **`id`**          | `integer`          | `not null, primary key`
# **`updated_at`**  | `datetime`         | `not null`
# **`user_id`**     | `integer`          |
#
# ### Indexes
#
# * `index_chats_on_button_id`:
#     * **`button_id`**
# * `index_chats_on_user_id`:
#     * **`user_id`**
#
# ### Foreign Keys
#
# * `fk_rails_...`:
#     * **`button_id => buttons.id`**
# * `fk_rails_...`:
#     * **`user_id => users.id`**
#

class Chat < ApplicationRecord
  belongs_to :user, required: true
  belongs_to :button, required: true
  has_one :button_creator, through: :button, source: :creator

  has_many :messages, dependent: :destroy

  scope :sharer?, ->(user=nil) { unless user.nil?
    where 'user_id = ?', user
  end }

  def chatters
    [user, button_creator]
  end

  def unread_messages(user)
    messages.unread_by_user(user).count
  end

end
