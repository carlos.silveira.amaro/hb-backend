# ## Schema Information
#
# Table name: `images`
#
# ### Columns
#
# Name                     | Type               | Attributes
# ------------------------ | ------------------ | ---------------------------
# **`created_at`**         | `datetime`         | `not null`
# **`file_content_type`**  | `string`           |
# **`file_file_name`**     | `string`           |
# **`file_file_size`**     | `integer`          |
# **`file_updated_at`**    | `datetime`         |
# **`icon_id`**            | `integer`          |
# **`id`**                 | `integer`          | `not null, primary key`
# **`image_property`**     | `string`           |
# **`imageable_id`**       | `integer`          |
# **`imageable_type`**     | `string`           |
# **`provisional`**        | `boolean`          | `default(FALSE)`
# **`updated_at`**         | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_images_on_icon_id`:
#     * **`icon_id`**
# * `index_images_on_imageable_id`:
#     * **`imageable_id`**
#

class Image < ApplicationRecord

  belongs_to :imageable, polymorphic: true

  has_attached_file :file, styles: lambda{ |attachment| attachment.instance.create_hash_styles }
  validates_attachment_content_type :file, content_type: /\Aimage/
  validates_attachment_file_name :file, matches: [/png\Z/, /jpe?g\Z/]

  # validates_presence_of :imageable_id, :imageable_type, :image_property
  # validate :imageable_type_id_existence

  IMAGES_FORMATS = ["image/jpg","image/jpeg","image/png"]

  IMAGES_SIZES = {medium: '500x>', thumb: '300x>', small: '150x>', tiny: '50x>'}

  BELONGS_TO_PROPERTIES = { user: ['avatar'], button: ['image'] }

  LIMIT_BELONGS_TO_IMAGES = 1

  after_create :remove_previous_images

  def asset_url_normal
    "#{self.file.url}"
  end

  def asset_url(size)
    "#{self.file.url(size.to_sym)}"
  end

  def create_hash_styles
    IMAGES_FORMATS.include?(self.file_content_type) ? IMAGES_SIZES : {}
  end

  def remove_previous_images
    properties = BELONGS_TO_PROPERTIES[self.imageable_type.downcase.to_sym]
    if properties.present? && properties.include?(self.image_property)
      images = Image.where(imageable_id:self.imageable_id,imageable_type:self.imageable_type,image_property:self.image_property)
      if images.present? && images.count > LIMIT_BELONGS_TO_IMAGES
        selected_image = images.where.not(id:self.id)
        selected_image.destroy_all
      end
    end
  end

  # def imageable_type_id_existence
  #   imageable_types  = imageable_type_availables
  #   self.errors.add(:"imageable-type", I18n.t('activerecord.errors.image.imageable_type_not_found',imageable_type: self.imageable_type)) unless imageable_types.include?(self.imageable_type)
  #   self.errors.add(:"imageable-id", I18n.t('activerecord.errors.image.imageable_id_not_found')) if self.imageable_id.blank?
  # end

  # def imageable_type_availables
  #   ActiveRecord::Base.connection.tables.map do |model|
  #     model.capitalize.singularize.camelize
  #   end
  # end

end
