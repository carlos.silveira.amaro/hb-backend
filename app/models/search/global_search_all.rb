class Search::GlobalSearchAll < Search::GlobalSearch

  def initialize(model,params)
    super(params)
  end

  def execute_search(model)
    self.successful_search = self.errors.empty?
    self.results = eval(model).all.paginate(page:@page,per_page: @size) if self.successful_search
    self
  end

end
