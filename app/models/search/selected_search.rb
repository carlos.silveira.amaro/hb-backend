class Search::SelectedSearch < Search::GeoCodeSearch
  def initialize(search_params)
    super(search_params)
    @ids = @params[:id]
  end

  def search_buttons
    results = Button.where(id:@ids)
    return results,{}
  end

end
