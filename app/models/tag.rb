# ## Schema Information
#
# Table name: `tags`
#
# ### Columns
#
# Name                          | Type               | Attributes
# ----------------------------- | ------------------ | ---------------------------
# **`active_buttons_counter`**  | `integer`          |
# **`buttons_counter`**         | `integer`          |
# **`created_at`**              | `datetime`         | `not null`
# **`id`**                      | `integer`          | `not null, primary key`
# **`name`**                    | `string`           |
# **`search_name`**             | `string`           |
# **`updated_at`**              | `datetime`         | `not null`
#

class Tag < ApplicationRecord
  has_many :button_tags
  has_many :buttons, :through => :button_tags, source: :button
  has_many :button_net_tags
  has_many :button_nets, :through => :button_net_tags, source: :button_net
  has_many :user_tags
  has_many :users, :through => :user_tags, source: :user

  validates :name, presence: true, uniqueness: true

  after_create :update_counter
  after_update :update_counter


  def update_counter
    allButtons = self.buttons
    activeButtons = []

    allButtons.each do |param|
      if (param.active && !param.deactivated_user)
        activeButtons.push(param)
      end
    end

    if(self.active_buttons_counter != activeButtons.count)
      self.active_buttons_counter = activeButtons.count
      self.save
    end

  end
end
