# ## Schema Information
#
# Table name: `transfered_buttons`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`button_id`**   | `integer`          | `not null`
# **`created_at`**  | `datetime`         | `not null`
# **`email`**       | `string`           | `not null`
# **`id`**          | `integer`          | `not null, primary key`
# **`updated_at`**  | `datetime`         | `not null`
# **`user_id`**     | `integer`          | `not null`
#

class TransferedButton < ApplicationRecord
end
