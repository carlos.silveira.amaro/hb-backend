# ## Schema Information
#
# Table name: `users`
#
# ### Columns
#
# Name                          | Type               | Attributes
# ----------------------------- | ------------------ | ---------------------------
# **`active`**                  | `boolean`          | `default(TRUE)`
# **`admin`**                   | `boolean`          | `default(FALSE)`
# **`app_language`**            | `string`           |
# **`avatar_id`**               | `integer`          |
# **`blocked`**                 | `integer`          | `default([]), is an Array`
# **`blocked_by`**              | `integer`          | `default([]), is an Array`
# **`created_at`**              | `datetime`         | `not null`
# **`current_sign_in_at`**      | `datetime`         |
# **`current_sign_in_ip`**      | `inet`             |
# **`deliver_interests`**       | `boolean`          | `default(FALSE)`
# **`description`**             | `string`           |
# **`email`**                   | `string`           | `default(""), not null`
# **`encrypted_password`**      | `string`           | `default(""), not null`
# **`id`**                      | `integer`          | `not null, primary key`
# **`last_sign_in_at`**         | `datetime`         |
# **`last_sign_in_ip`**         | `inet`             |
# **`liked`**                   | `integer`          | `default([]), is an Array`
# **`likes`**                   | `integer`          | `default(0)`
# **`location`**                | `string`           |
# **`name`**                    | `string`           |
# **`nickname`**                | `string`           |
# **`phone`**                   | `string`           |
# **`provider_id`**             | `string`           |
# **`provider_name`**           | `string`           |
# **`remember_created_at`**     | `datetime`         |
# **`reset_password_sent_at`**  | `datetime`         |
# **`reset_password_token`**    | `string`           |
# **`show_phone`**              | `boolean`          | `default(FALSE)`
# **`sign_in_count`**           | `integer`          | `default(0), not null`
# **`super_admin`**             | `boolean`          | `default(FALSE)`
# **`tags_distance`**           | `integer`          | `default(20)`
# **`tags_latitude`**           | `float`            |
# **`tags_longitude`**          | `float`            |
# **`updated_at`**              | `datetime`         | `not null`
# **`use_external_conv`**       | `boolean`          | `default(FALSE)`
# **`use_telegram`**            | `boolean`          | `default(FALSE)`
# **`use_whatsapp`**            | `boolean`          | `default(FALSE)`
# **`user_telegram`**           | `string`           |
#
# ### Indexes
#
# * `index_users_on_avatar_id`:
#     * **`avatar_id`**
# * `index_users_on_email` (_unique_):
#     * **`email`**
# * `index_users_on_reset_password_token` (_unique_):
#     * **`reset_password_token`**
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :token_authenticatable

  has_many :owned_buttons, class_name: 'Button', inverse_of: :creator, foreign_key: :creator_id, dependent: :destroy
  has_many :button_chats, through: :owned_buttons, source: :creator
  has_many :chats, inverse_of: :user
  has_many :buttons, through: :chats, source: :button
  has_many :authentication_tokens, dependent: :destroy
  has_many :user_tags
  # has_many :interests, through: :user_tags, source: :user_tag
  has_one :avatar, class_name: 'Image', foreign_key: 'imageable_id',as: :imageable, dependent: :destroy
  has_and_belongs_to_many :languages

  has_many :button_nets, class_name: 'ButtonNet', inverse_of: :creator, foreign_key: :creator_id, dependent: :destroy
  # has_many :button_nets, through: :admin_users, source: :button_net
  # has_many :button_nets, through: :blocked_users, source: :button_net



  attr_accessor :current_token, :random_password

  def self.find_with_token email, token
    self.find_by email: email, authentication_tokens: { body: token }
  end

  def account_allowed?
       blocked_at.nil?
  end

  def inactive_message
      account_active? ? super : :locked
  end

  scope :notifications_number?, -> {
    this.get_notifications_number
  }

  def get_notifications_number
    notifNumber = 0
    self.owned_buttons.each do |button|
      button.chats do |chat|
        notifNumber = notifNumber + chat.unread_messages(self)
      end
    end
    notifNumber
  end

  def get_button_with_msg
    buttonCounter = 0
    self.owned_buttons.each do |button|
      button.chats.each do |chat|
        if chat.messages.count >= 1
          buttonCounter = buttonCounter + 1
          break
        end
      end
    end
    buttonCounter
  end

  def get_pressed_btn_counter
    buttonCounter = 0
    self.chats.each do |chat|
      if chat.user_id != self.id
        buttonCounter = buttonCounter + 1
        break
      end
    end
    buttonCounter
  end
end
