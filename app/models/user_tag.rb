# ## Schema Information
#
# Table name: `user_tags`
#
# ### Columns
#
# Name               | Type               | Attributes
# ------------------ | ------------------ | ---------------------------
# **`button_nets`**  | `integer`          | `default([]), is an Array`
# **`created_at`**   | `datetime`         | `not null`
# **`id`**           | `integer`          | `not null, primary key`
# **`tag_id`**       | `integer`          |
# **`tag_type`**     | `integer`          |
# **`updated_at`**   | `datetime`         | `not null`
# **`user_id`**      | `integer`          |
#
# ### Indexes
#
# * `index_user_tags_on_tag_id`:
#     * **`tag_id`**
# * `index_user_tags_on_user_id`:
#     * **`user_id`**
#

class UserTag < ApplicationRecord
  belongs_to :user
  belongs_to :tag

  scope :interested, -> { where(tag_type: 2) }

  # after_commit :update_tag_counter
  #
  # def update_tag_counter
  #   self.tag.update_counter
  # end

end
