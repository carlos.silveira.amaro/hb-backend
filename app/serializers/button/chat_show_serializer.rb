class Button::ChatShowSerializer < ActiveModel::Serializer
  # Common attributes
  attributes :description, :button_type, :created_at, :swap, :date, :shared_counter

  # Location attributes
  attributes :full_address, :location_name, :to_location_name, :latitude, :longitude, :to_latitude, :to_longitude

  attribute :chats_counter do
    object.chats.count
  end

  has_many :offer_tags, serializer: Tag::ShowSerializer

  has_many :needed_tags, serializer: Tag::ShowSerializer

  has_one :image

  has_many :chats do
    include_data false
    # link :self, "/api/v1/buttons/#{object.id}/relationships/chats"
    link :related,  "/api/v1/buttons/#{object.id}/chats"
  end

end
