class ButtonNets::ShowSerializer < ActiveModel::Serializer
  # Common attributes
  attributes :name, :description, :active, :img_url, :latitude, :location_name, :longitude, :net_url, :privacy, :updated_at, :other_net_ids

  # Location attributes
  # attributes :full_address, :location_name, :to_location_name, :latitude, :longitude, :to_latitude, :to_longitude

  has_one :creator, serializer: User::ShowSerializer

  has_many :tags, serializer: Tag::IndexSerializer

  attribute :blocked_users do
    @blocked_users = []
    if !current_user.nil?
      if current_user.id == object.creator.id
        @blocked_users = object.blocked_users
      else
        if object.blocked_users.include? current_user.id
          @blocked_users.push(current_user.id)
        end
      end
    end
    @blocked_users
  end

  attribute :admin_users do
    @admin_users = []
    if !current_user.nil?
      if current_user.id == object.creator.id
        @admin_users = object.admin_users
      end
    end
    @admin_users
  end

  attribute :allowed_users do
    @allowed_users = []
    if !current_user.nil?
      if current_user.id == object.creator.id
        @allowed_users = object.allowed_users
      else
        if object.allowed_users.include? current_user.id
          @allowed_users.push(current_user.id)
        end
      end
    end
    @allowed_users
  end

  attribute :friend_nets_id do
    @friend_nets_id = []
    if !current_user.nil?
      if current_user.id == object.creator.id
        @friend_nets_id = object.friend_nets_id
      end
    end
    @friend_nets_id
  end

  # has_many :offer_tags, serializer: Tag::ShowSerializer
  #
  # has_many :needed_tags, serializer: Tag::ShowSerializer

end
