class ButtonSerializer < ActiveModel::Serializer
  # Common attributes
  attributes :description, :button_type, :active, :created_at, :swap, :date, :shared_counter, :periodic_date

  # Location attributes
  attributes :full_address, :location_name, :to_location_name, :latitude, :longitude, :to_latitude, :to_longitude


  has_one :creator, serializer: User::ShowSerializer

  has_one :image
end
