class User::ChatShowSerializer < ActiveModel::Serializer
  attributes :nickname, :active

  has_one :avatar

  # has_one :avatar do
  #   include_data true
  # end

end
