class User::ShowSerializer < ActiveModel::Serializer
  attributes :id, :nickname, :email, :description, :location, :show_phone, :user_telegram, :use_telegram, :use_whatsapp, :use_external_conv, :likes

  attribute :phone do
    if object.show_phone
      object.phone
    end
  end

  attribute :user_telegram do
    if object.use_telegram
      object.user_telegram
    end
  end

  attribute :owned_buttons_counter do
    object.owned_buttons.shown?(true).count
  end

  # has_one :avatar do
  #   include_data true
  # end

  has_many :owned_buttons, serializer: Button::UserShowSerializer do
    object.owned_buttons.shown?(true)
  end

  has_one :avatar
  has_many :user_tags, serializer: UserTag::ShowSerializer
  has_many :languages, serializer: Language::IndexSerializer

end
