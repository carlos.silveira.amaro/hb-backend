class Application

  class << self
    def exec *args
      self.new(*args).exec
    end
  end

  def initialize
    raise NotImplementedError, "Subclasses must define `initialize`."
  end

  def exec
    raise NotImplementedError, "Subclasses must define `exec`."
  end

end
