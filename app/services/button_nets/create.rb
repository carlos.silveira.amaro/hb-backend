class ButtonNets::Create < Application

  attr_reader :params, :current_user

  def initialize params, current_user
    @params = params
    @current_user = current_user
  end

  def exec
    build_basic_button_net
  end

  protected
  def build_basic_button_net
    button_net = ButtonNet.new button_net_params
    button_net.creator = current_user
    # button_net.location_name = "Test"
    button_net.save
    button_net
  end

  def button_net_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[ name description active img-url latitude location-name longitude net-url privacy updated-at net-options creator-id]
  end
end
