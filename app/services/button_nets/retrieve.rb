class ButtonNets::Retrieve < Application

  attr_reader :params

  def initialize params
    @params = params
  end

  def exec
    retrieve_button_nets
  end

  protected
  def retrieve_button_nets
    button_nets = []
    unless params[:data].nil?
      params[:data].each do |button_net|
        render_errors(I18n.t("activerecord.errors.button.button_nets.wrong_type"), I18n.t("activerecord.errors.button.button_nets.wrong_type_description"), 403) and return unless %w{button_nets}.include? button_net[:type]
        new_button_net = ButtonNet.find(button_net[:id])
        button_nets << new_button_net
      end
    end
    return button_nets
  end

end
