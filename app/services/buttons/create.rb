class Buttons::Create < Application

  attr_reader :params, :current_user

  def initialize params, current_user
    @params = params
    @current_user = current_user
  end

  def exec
    build_basic_button
  end

  protected
  def build_basic_button
    button = Button.create button_params
    button.creator = current_user
    button
  end

  def button_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[ description button-type active full-address latitude longitude to-latitude to-longitude swap offer-tags needed-tags location-name to-location-name date periodic-date button-net-id ]
  end

end
