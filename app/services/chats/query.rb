class Chats::Query < Application

  attr_reader :params, :current_user

  def initialize params, current_user
    @params = params
    @current_user = current_user
  end

  def exec
    query_chat
  end

  protected
  def query_chat
    Chat.find_by chat_params
  end

  def chat_params
    {
      button_id: params[:button],
      user: current_user
    }
  end
end
