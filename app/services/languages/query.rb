class Languages::Query < Application

  attr_reader :params

  def initialize params
    @params = params
  end

  def exec
    query_language
  end

  protected
  def query_language
    if language_params[:filter]
      selected_langs = []
      unless language_params[:name].blank?
        Language.all.each do |tl|
          selected_langs << tl.id if I18n.transliterate(tl.translated_name.downcase).include? (I18n.transliterate(language_params[:name].downcase))
        end
      end
      languages = Language.where(id: selected_langs)
    else
      languages = Language.all
    end
    return languages
  end

  def language_params
    {
      filter: !params.try(:[], :filter).blank?,
      name: params.try(:[], :filter).try(:[], :name)
    }
  end

end
