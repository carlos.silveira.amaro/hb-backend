class Resources::Get < Application

  attr_reader :params, :class_name

  def initialize params
    @id = params[:id]
    @class = params[:controller].include?("/") ? eval(params[:controller].split("/").first.slice(0..-2).camelize) : eval(params[:controller].slice(0..-2).camelize)
  end

  def exec
    get_resource
  end

  protected
  def get_resource
    resource = @class.find_by_id(@id)
    resource.present? ? resource : nil
  end

end
