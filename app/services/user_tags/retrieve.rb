class UserTags::Retrieve < Application

  attr_reader :params

  def initialize params
    @params = params
  end

  def exec
    retrieve_tags
  end

  protected
  def retrieve_tags
    tags = []
    unless params[:data].nil?
      params[:data].each do |tag|
        render_errors(I18n.t("activerecord.errors.button.tags.wrong_type"), I18n.t("activerecord.errors.button.tags.wrong_type_description"), 403) and return unless %w{tags}.include? tag[:type]
        new_tag = UserTag.find(tag[:id])
        tags << new_tag
      end
    end
    return tags
  end

end
