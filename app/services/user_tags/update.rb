class UserTags::Update < Application

  attr_reader :params, :tag

  def initialize params, tag
    @params = params
    @tag = tag
  end

  def exec
    update_tag
  end

  def params
    @params.to_unsafe_hash
  end

  protected
  def update_tag
    tag.assign_attributes tag_params
    tag
  end

  def tag_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[tag-type button-nets]
  end

end
