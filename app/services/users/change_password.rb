class Users::ChangePassword < Application

  attr_reader :params, :user

  def initialize params, user
    @params = params
    @user   = user
  end

  def exec
    change_password
  end

  protected
  def change_password
    user.assign_attributes password_params
    user
  end

  def password_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[password password_confirmation]
  end
end
