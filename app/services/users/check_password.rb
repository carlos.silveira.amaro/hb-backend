class Users::CheckPassword < Application

  attr_reader :params, :user

  def initialize params, user
    @params = params
    @user   = user
  end

  def exec
    check_password
  end

  protected
  def check_password
    user.errors.add(:base, I18n.t('activerecord.errors.users.password.fill_all_fields')) unless password_params[:current_password].present? && password_params[:password].present? && password_params[:password_confirmation].present? 
    user.errors.add(:current_password, I18n.t('activerecord.errors.users.password.current_validation')) unless user.valid_password?(password_params[:current_password])
    user.errors.add(:password, I18n.t('activerecord.errors.users.password.current_new_validation')) unless password_params[:current_password] != password_params[:password]
    user.errors.add(:password, I18n.t('activerecord.errors.users.password.not_same', param: I18n.t('activerecord.attributes.user.password_confirmation'))) and user.errors.add(:password_confirmation, I18n.t('activerecord.errors.users.password.not_same', param: I18n.t('activerecord.attributes.user.new_password'))) unless password_params[:password] == password_params[:password_confirmation]
    user
  end

  def password_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[current-password password password-confirmation]
  end
end
