require 'securerandom'

class Users::Create < Application

  attr_reader :params

  def initialize params
    @params = params
  end

  def exec
    register_user
  end

  def params
    @params.to_unsafe_hash
  end

  protected
  def register_user
    user = User.new user_params
    if User.last.nil?
      user.nickname = user.email.split('@')[0].capitalize+(1).to_s
      user.name = user.email.split('@')[0].capitalize+(1).to_s
    else
      user.nickname = user.email.split('@')[0].capitalize+(User.last.id).to_s
      user.name = user.email.split('@')[0].capitalize+(User.last.id).to_s
    end
    user.password = user.password_confirmation = SecureRandom.hex(6)
    # user.provider_name = "Helpbuttons"
    user
  end

  def user_params
    ActiveModelSerializers::Deserialization.jsonapi_parse params, only: allowed_params
  end

  def allowed_params
    %i[email nickname avatar]
  end
end
