class Users::Search < Application

  attr_reader :params, :user, :current_user

  def initialize params, current_user
    @params = params
    if !current_user.nil?
      @current_user = current_user
    end
  end

  def exec
    users,meta_info = search_user
    if !@current_user.nil? && @current_user.admin
      users = buttons.shown_admin?(true, @current_user) unless buttons.blank?
    elsif !@current_user.nil? && @current_user.super_admin
      users = buttons.shown_super_admin?(true) unless buttons.blank?
    else
      users = buttons.shown?(true) unless buttons.blank?
    end
    # dayButtons = []
    # buttons.each do |button|
    #   if button.periodic_date == nil
    #     dayButtons.push(button)
    #   else
    #     if button.periodic_date.include? 'Lunes'
    #       if Date.today.strftime('%A') == "Monday"
    #         dayButtons.push(button)
    #       end
    #     end
    #     if button.periodic_date.include? 'Martes'
    #       if Date.today.strftime('%A') == "Tuesday"
    #         dayButtons.push(button)
    #       end
    #     end
    #     if button.periodic_date.include? 'Miercoles'
    #       if Date.today.strftime('%A') == "Wednesday"
    #         dayButtons.push(button)
    #       end
    #     end
    #     if button.periodic_date.include? 'Jueves'
    #       if Date.today.strftime('%A') == "Thursday"
    #         dayButtons.push(button)
    #       end
    #     end
    #     if button.periodic_date.include? 'Viernes'
    #       if Date.today.strftime('%A') == "Friday"
    #         dayButtons.push(button)
    #       end
    #     end
    #     if button.periodic_date.include? 'Sabado'
    #       if Date.today.strftime('%A') == "Saturday"
    #         dayButtons.push(button)
    #       end
    #     end
    #     if button.periodic_date.include? 'Domingo'
    #       if Date.today.strftime('%A') == "Sunday"
    #         dayButtons.push(button)
    #       end
    #     end
    #     # dayButtons.push(button)
    #   end
    # end
    # return dayButtons,meta_info
    return buttons,meta_info
  end

  def params
    @params.to_unsafe_hash
  end

  protected
  def search_button
    search_type = ::Search::GeoCodeSearch.search_type(search_params)
    searcher = ::Search::GeoCodeSearch.get_searcher(search_type,search_params)
    searcher.search_buttons
  end

  def search_params
    ActionController::Parameters.new(params).permit(allowed_params)
  end

  def allowed_params
    %i[ name email id ]
  end

end
