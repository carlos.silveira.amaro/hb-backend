Rails.application.routes.draw do

  scope :api, defaults:{ :format => 'json' } do
      scope :v1 do

         post 'token', to: 'tokens#create'

         devise_for :users, skip: [:passwords, :registrations],
                    controllers: {sessions: 'users/sessions'}

         devise_scope :user do
           post "users", to: 'users/registrations#create', as: 'user_registration'
           post "recovery", to: 'users/passwords#create', as: 'user_recovery'
           patch "password/reset/:token", to: 'users/passwords#reset', as: 'reset_user_password'
         end

        resources :users, only: [:index, :show, :update]

        namespace :users do
          scope :":id" do
            patch :password, as: 'change_password'
            scope :relationships do
              patch :"interests", to: 'tags#update_interests'
              patch :"languages", to: 'languages#update_languages'
            end
            patch '/like', action: 'update_likes'
            patch '/block', action: 'update_blocked_by'
          end
          get '/:email', action: 'show'
          delete '/:id', action: 'remove_profile'
        end

        resources :buttons

        namespace :buttons do
          scope :":id" do
            get :chats, to: 'chats#sharer_chats'
            scope :relationships do
              get :chats, to: 'chats#sharer_chats_relation'
              post :"offer-tags", to: 'tags#add_offer_tags'
              patch :"offer-tags", to: 'tags#update_offer_tags'
              delete :"offer-tags", to: 'tags#remove_offer_tags'
              post :"needed-tags", to: 'tags#add_needed_tags'
              patch :"needed-tags", to: 'tags#update_needed_tags'
              delete :"needed-tags", to: 'tags#remove_needed_tags'
              patch :"creator", to: 'users#transfer_button_to'
            end
          end
        end

        # resources :button_nets
        get 'button-nets', to: 'button_nets#index'
        get 'button-nets/:id', to: 'button_nets#show'
        post 'button-nets', to: 'button_nets#create'
        patch 'button-nets/:id', to: 'button_nets#update'
        patch 'button-nets/:id/allow', to: 'button_nets#allow'
        delete 'button-nets/:id', to: 'button_nets#destroy'

        resources :languages, only: [:index, :show]

        resources :tags, except: [:update, :destroy]

        # resources :userTags
        get 'user-tags', to: 'user_tags#index'
        get 'user-tags/:id', to: 'user_tags#show'
        post 'user-tags', to: 'user_tags#create'
        patch 'user-tags/:id', to: 'user_tags#update'
        delete 'user-tags/:id', to: 'user_tags#destroy'

        resources :chats, except: [:update]

        namespace :chats do
          scope :":id" do
            get :messages, to: 'messages#index'
          end
        end

        resources :messages, except: [:update, :destroy]

        resources :images

    end
  end

  get '(*any)', to: 'home#index'

end
