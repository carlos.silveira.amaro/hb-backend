class CreateButtons < ActiveRecord::Migration[5.0]
  def change
    create_table :buttons do |t|
      t.text :description
      t.string :full_address
      t.string :date
      t.string :location_name
      t.float :latitude
      t.float :longitude
      t.float :to_latitude
      t.float :to_longitude
      t.integer :button_type, default: 0
      t.boolean :active, default: false
      t.boolean :deactivated_user, default: false
      t.boolean :swap
      t.references :creator, references: :users
      t.timestamps
    end

    add_index :buttons, :button_type
    add_foreign_key :buttons, :users, column: :creator_id
  end
end
