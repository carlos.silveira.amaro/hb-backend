class AddShowPhoneToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :show_phone, :boolean, default: false
  end
end
