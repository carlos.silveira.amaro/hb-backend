class AddUseExternalConvToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :use_external_conv, :boolean, default:false
  end
end
