class AddUseWhatsappToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :use_whatsapp, :boolean, default:false
  end
end
