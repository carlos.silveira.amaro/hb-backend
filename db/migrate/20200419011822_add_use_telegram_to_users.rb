class AddUseTelegramToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :use_telegram, :boolean, default:false
  end
end
