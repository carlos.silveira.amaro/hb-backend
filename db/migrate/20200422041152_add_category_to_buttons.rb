class AddCategoryToButtons < ActiveRecord::Migration[5.0]
  def change
    add_reference :buttons, :button_category, references: :categories
  end
end
