class AddDeliverInterestsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :deliver_interests, :boolean, default: false
  end
end
