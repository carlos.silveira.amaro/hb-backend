class AddSearchNameToTags < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :search_name, :string
  end
end
