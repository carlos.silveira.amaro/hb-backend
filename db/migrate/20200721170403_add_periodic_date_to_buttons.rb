class AddPeriodicDateToButtons < ActiveRecord::Migration[5.0]
  def change
    add_column :buttons, :periodic_date, :string, array: true, default: '{}'
  end
end
