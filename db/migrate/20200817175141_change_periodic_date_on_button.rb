class ChangePeriodicDateOnButton < ActiveRecord::Migration[5.0]
  def change
    change_column :buttons, :periodic_date, :string, array: false, default: nil
  end
end
