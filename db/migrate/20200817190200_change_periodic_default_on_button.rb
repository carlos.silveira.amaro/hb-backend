class ChangePeriodicDefaultOnButton < ActiveRecord::Migration[5.0]
  def change
    change_column :buttons, :periodic_date, :string, default: nil
  end
end
