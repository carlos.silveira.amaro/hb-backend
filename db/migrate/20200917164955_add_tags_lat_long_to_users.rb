class AddTagsLatLongToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :tags_latitude, :float, default: ""
    add_column :users, :tags_longitude, :float, default: ""
  end
end
