class FixColumnCategoryName < ActiveRecord::Migration[5.0]
  def change
    rename_column :buttons, :button_category_id, :button_nets_id
  end
end
