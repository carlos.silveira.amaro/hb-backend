class AddDetailsToButtonNets < ActiveRecord::Migration[5.0]
  def change
    add_column :button_nets, :blocked_users, :integer
    add_column :button_nets, :admin_users, :integer
    add_column :button_nets, :net_tags, :integer
  end
end
