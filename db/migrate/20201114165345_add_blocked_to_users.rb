class AddBlockedToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :blocked, :integer, array:true, default: []
    add_column :users, :blocked_by, :integer, array:true, default: []
    add_column :users, :likes, :integer, default: 0
    add_column :users, :liked, :integer, array:true, default: []
  end
end
