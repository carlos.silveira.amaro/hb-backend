class ChangeNetColumnType < ActiveRecord::Migration[5.0]
  def change
    def up
      change_column :button_nets, :net_options, :boolean
    end

    def down
      change_column :my_table, :my_column, :integer
    end
  end
end
