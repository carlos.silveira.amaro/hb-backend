class ChangeIntegerAttrToButtonNets < ActiveRecord::Migration[5.0]
  def change
    change_column_null :button_nets, :net_url, true
    remove_column :button_nets, :blocked_users
  end
end
