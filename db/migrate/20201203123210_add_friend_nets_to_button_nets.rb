class AddFriendNetsToButtonNets < ActiveRecord::Migration[5.0]
  def change
    add_column :button_nets, :friend_nets_id, :integer, array:true, default: []
  end
end
