class AddAllowedUsersToButtonNets < ActiveRecord::Migration[5.0]
  def change
    add_column :button_nets, :allowed_users, :integer, array:true, default: []
  end
end
