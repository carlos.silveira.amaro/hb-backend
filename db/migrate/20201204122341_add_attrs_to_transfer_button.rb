class AddAttrsToTransferButton < ActiveRecord::Migration[5.0]
  def change
    add_column :transfered_buttons, :button_id, :integer, null: false
    add_column :transfered_buttons, :user_id, :integer, null: false
    add_column :transfered_buttons, :email, :string, null: false
  end
end
