class CheangeButtonNetIdRelation < ActiveRecord::Migration[5.0]
  def change
    remove_column :buttons, :button_nets_id, :button_net_id
    add_reference :buttons, :button_net_id, references: :button_nets
  end
end
