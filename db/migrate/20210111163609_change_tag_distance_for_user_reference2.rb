class ChangeTagDistanceForUserReference2 < ActiveRecord::Migration[5.0]
  def change
    add_reference :buttons, :button_net, references: :button_nets
  end
end
