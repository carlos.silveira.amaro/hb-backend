class RemoveNetTAgsFromButtonNet < ActiveRecord::Migration[5.0]
  def change
    remove_column :button_nets, :net_tags
  end
end
