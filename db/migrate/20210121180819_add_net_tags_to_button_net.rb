class AddNetTagsToButtonNet < ActiveRecord::Migration[5.0]
  def change
    add_reference :button_nets, :tags, references: :tags
  end
end
