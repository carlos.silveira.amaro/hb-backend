class CreateButtonNetTags < ActiveRecord::Migration[5.0]
  def change
    create_table :button_net_tags do |t|
      t.references :button_net
      t.references :tag
      t.timestamps
    end
  end
end
