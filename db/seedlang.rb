# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

puts "\n\t\t*************************************************************************\n\n"
puts "\t\t ##   ## ####### ##     #######      #######  ######## ##    ##  #####  "
puts "\t\t ##   ## ##      ##     ##    ##     ##    ##    ##    ###   ## ##   ## "
puts "\t\t ##   ## ##      ##     ##    ##     ##    ##    ##    ####  ## ##      "
puts "\t\t ####### #####   ##     #######      #######     ##    ## ## ##  #####  "
puts "\t\t ##   ## ##      ##     ##           ##    ##    ##    ##  ####      ## "
puts "\t\t ##   ## ##      ##     ##           ##    ##    ##    ##   ### ##   ## "
puts "\t\t ##   ## ####### ###### ##           #######     ##    ##    ##  #####  "
puts "\n\n\t\t\t      HELP BUTTONS - POPULATE DATABASE EXAMPLE\n\n"
puts "\t\t*************************************************************************\n"

puts "\n\t\t- Creating languages..."
languages = []
{ "af" => "Afrikaans", "sq" => "Albanian",  "ar" => "Arabic", "eu" => "Basque", "bg" => "Bulgarian",  "be" => "Byelorussian", "ca" => "Catalan",
"da" => "Danish", "nl" => "Dutch",  "en" => "English", "eo" => "Esperanto",  "et" => "Estonian", "fo" => "Faroese", "fi" => "Finnish",
"fr" => "French", "gl" => "Galician",  "de" => "German", "el" => "Greek", "iw" => "Hebrew", "hu" => "Hungarian",  "is" => "Icelandic",
"ga" => "Irish",  "it" => "Italian",  "ja" => "Japanese", "ko" => "Korean",  "lv" => "Latvian", "lt" => "Lithuanian",  "mk" => "Macedonian",
"mt" => "Maltese",  "no" => "Norwegian", "pl" => "Polish", "pt" => "Portuguese",  "ro" => "Romanian",  "ru" => "Russian",  "gd" => "Scottish",
"sr" => "Serbian", "sk" => "Slovak",  "sl" => "Slovenian",  "es" => "Spanish", "sv" => "Swedish",  "tr" => "Turkish", "uk" => "Ukrainian",
"hr" => "Croatian",  "cs" => "Czech"}.each do |code, name|
  languages << Language.create(code: code, name: name)
end

puts "\n\n\t*************************************************************************"
puts "\t\t     HELP BUTTONS - END POPULATE DATABASE"
puts "\t*************************************************************************\n\n\n"
