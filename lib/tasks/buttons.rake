namespace :buttons do
  desc "Notificar botones pasados de fecha"
  task notify_outdated: :environment do
    Button.all.each do |button|
      if !button.date.nil? && button.date != "Ahora" && button.mail_schedule_sended != true
        button.update(mail_schedule_sended: true)
        if button.date.to_date == Time.zone.today
          button.chats.each do |chat|
            UserNotifierMailer.outdated_button(chat.user, button).deliver
          end
        end
        if button.date.to_date < Time.zone.today
          button.update(active: false)
        end
        if button.date.to_date < Time.zone.today + 60 && button.active == false
          button.destroy
        end
      end
    end
  end
end
