namespace :languages do

  task generate: :environment do
    { "af" => "Afrikaans", "sq" => "Albanian",  "ar" => "Arabic", "eu" => "Basque", "bg" => "Bulgarian",  "be" => "Byelorussian", "ca" => "Catalan",  
      "da" => "Danish", "nl" => "Dutch",  "en" => "English", "eo" => "Esperanto",  "et" => "Estonian", "fo" => "Faroese", "fi" => "Finnish",  
      "fr" => "French", "gl" => "Galician",  "de" => "German", "el" => "Greek", "iw" => "Hebrew", "hu" => "Hungarian",  "is" => "Icelandic", 
      "ga" => "Irish",  "it" => "Italian",  "ja" => "Japanese", "ko" => "Korean",  "lv" => "Latvian", "lt" => "Lithuanian",  "mk" => "Macedonian", 
      "mt" => "Maltese",  "no" => "Norwegian", "pl" => "Polish", "pt" => "Portuguese",  "ro" => "Romanian",  "ru" => "Russian",  "gd" => "Scottish", 
      "sr" => "Serbian", "sk" => "Slovak",  "sl" => "Slovenian",  "es" => "Spanish", "sv" => "Swedish",  "tr" => "Turkish", "uk" => "Ukrainian",
      "hr" => "Croatian",  "cs" => "Czech"}.each do |code, name|
      Language.create(code: code, name: name)
    end
  end

end

